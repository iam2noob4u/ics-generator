<?php
// © 2017 Thomas Meester

namespace CalendarOutput;

use \Calendar\Calendar;
use \Calendar\Event;

class ICSOutput implements OutputInterface {
	const LINE_BREAK = "\r\n";
	const LINE_WIDTH = 74; // is 75, but we add a space when breaking - yes we COULD add one byte extra on the first line, but the complexity isn't worth it
	const DATE_FORMAT = 'Ymd\THis';

	const REPEAT_YEARLY_TEXT = 'YEARLY';
	const REPEAT_MONTHLY_TEXT = 'MONTHLY';
	const REPEAT_WEEKLY_TEXT = 'WEEKLY';
	const REPEAT_DAILY_TEXT = 'DAILY';
	const REPEAT_HOURLY_TEXT = 'HOURLY';
	const REPEAT_MINUTELY_TEXT = 'MINUTELY';
	const REPEAT_SECONDLY_TEXT = 'SECONDLY';

	const DAY_TEXT = [
		Event::DAY_SUNDAY => 'SU',
		Event::DAY_MONDAY => 'MO',
		Event::DAY_TUESDAY => 'TU',
		Event::DAY_WEDNESDAY => 'WE',
		Event::DAY_THURSDAY => 'TH',
		Event::DAY_FRIDAY => 'FR',
		Event::DAY_SATURDAY => 'SA',
	];

	protected static $RESOURCE_DIRECTORY = null;

	protected $outputTime;
	protected $uidCounter = 0;

	public function __construct() {
		if (static::$RESOURCE_DIRECTORY === null) {
			static::$RESOURCE_DIRECTORY = realpath(__DIR__ . '/../Resources/ICS');
		}
	}

	public function getHeaders():array {
		return [
			'Content-Type: "text/calendar; charset=UTF-8"',
		];
	}

	public function printCalendar(Calendar $cal, $out = STDOUT):bool {
		$this->outputTime = new \DateTime('now', new \DateTimeZone('UTC'));
		$this->uidCounter = 0;
		$this->writeBegin($out);
		$this->writeBasicInfo($cal, $out);
		$this->writeTimeZones($cal, $out);
		$this->writeEvents($cal->getEvents(), $out);
		$this->writeEnd($out);

		return true;
	}

	protected function escape(string $str):string {
		$str = preg_replace('/(:|;|"|,)/', '\\$0', $str);
		$str = preg_replace('/(\r|\n|\r\n)/', '\\n', $str);
		return $str;
	}

	protected function writeln($out, string $line) {
		$this->write($out, $line . static::LINE_BREAK);
	}

	protected function write($out, string $str) {
		$this->unsafeWrite($out, wordwrap($str, static::LINE_WIDTH, ' ' . static::LINE_BREAK, true));
	}

	protected function unsafeWrite($out, string $str) {
		fwrite($out, $str);
	}

	protected function writeBegin($out) {
		fwrite($out, <<<EOB
BEGIN:VCALENDAR
PRODID:-//Thomas' ICS Generator//Version 0.0.1//EN
VERSION:2.0
CALSCALE:GREGORIAN
METHOD:PUBLISH

EOB
		);
	}

	protected function writeEnd($out) {
		fwrite($out, 'END:VCALENDAR');
	}

	protected function writeBasicInfo(Calendar $cal, $out) {
		$this->writeln($out, 'X-WR-CALNAME:' . $this->escape($cal->getName()));
		$this->writeln($out, 'X-WR-TIMEZONE:' . $cal->getDefaultTimeZone()->getName());
		$this->writeln($out, 'X-WR-CALDESC:' . $this->escape($cal->getDescription()));
	}

	protected function writeTimeZones(Calendar $cal, $out) {
		$timeZones = [$cal->getDefaultTimeZone()];
		foreach ($cal->getEvents() as $event) {
			if (!in_array($event->getStartDate()->getTimeZone(), $timeZones)) {
				$timeZones[] = $event->getTimeZone();
			}
			if (!in_array($event->getEndDate()->getTimeZone(), $timeZones)) {
				$timeZones[] = $event->getTimeZone();
			}
		}

		foreach ($timeZones as $timeZone) {
			$this->writeTimeZone($timeZone, $out);
		}
	}

	protected function writeTimeZone(\DateTimeZone $tz, $out) {
		$this->unsafeWrite($out, file_get_contents(static::$RESOURCE_DIRECTORY . '/TimeZone/' . $tz->getName() . '.ics') . static::LINE_BREAK);
	}

	protected function writeEvents(array $events, $out) {
		foreach($events as $event) {
			$this->writeEvent($event, $out);
		}
	}

	protected function writeEvent(Event $event, $out) {
		$this->writeln($out, 'BEGIN:VEVENT');
		
		$this->writeln($out, 'DTSTART:' . $this->getDateText($event->getStartDate()));
		$this->writeln($out, 'DTEND:' . $this->getDateText($event->getEndDate()));
		
		$this->writeln($out, 'DTSTAMP:' . $this->getDateUTCText($this->outputTime, true));

		$this->writeln($out, 'UID:' . ($event->getUid() ?? $this->generateUID()));

		if ($event->getBasedOff() !== null && $event->getNormalStartTime() !== null) {
			$this->writeln($out, 'RECURRENCE-ID;' . $this->getDateText($event->getNormalStartTime()));
		} else {
			$this->writeEventRecurrance($event, $out);
		}

		$this->writeln($out, 'DESCRIPTION:' . $this->escape($event->getDescription() ?? ''));
		$this->writeln($out, 'SUMMARY:' . $this->escape($event->getName()));
		$this->writeln($out, 'TRANSP:OPAQUE');
		
		$this->writeln($out, 'END:VEVENT');
	}

	protected function writeEventRecurrance(Event $event, $out) {
		switch ($event->getRepeatMode()) {
			case Event::REPEAT_YEARLY:
				$repeatType = static::REPEAT_YEARLY_TEXT;
				break;
			case Event::REPEAT_MONTHLY:
				$repeatType = static::REPEAT_MONTHLY_TEXT;
				break;
			case Event::REPEAT_WEEKLY:
				$repeatType = static::REPEAT_WEEKLY_TEXT;
				break;
			case Event::REPEAT_DAILY:
				$repeatType = static::REPEAT_DAILY_TEXT;
				break;
			case Event::REPEAT_HOURLY:
				$repeatType = static::REPEAT_HOURLY_TEXT;
				break;
			case Event::REPEAT_MINUTELY:
				$repeatType = static::REPEAT_MINUTELY_TEXT;
				break;
			case Event::REPEAT_SECONDLY:
				$repeatType = static::REPEAT_SECONDLY_TEXT;
				break;
			case Event::REPEAT_NONE:
				return;
			default:
				throw new CalendarOutputException('Unknown repeat type: ' . $event->getRepeatMode() . '.');
		}
		
		$result = 'RRULE:FREQ=' . $repeatType;

		if ($event->getEndRepeat() instanceof \DateTime) {
			$dt = clone $event->getEndRepeat();
			$dt->setTimeZone(new \DateTimeZone('UTC'));
			$result .= ';UNTIL=' . $this->getDateUTCText($dt);
		} elseif (is_int($event->getEndRepeat() || ctype_digit($event->getEndRepeat()))) {
			$result .= ';COUNT=' . $event->getEndRepeat();
		}

		if ($event->getRepeatInterval() > 1) {
			$result .= ';INTERVAL=' . $event->getRepeatInterval();
		}

		if ($repeatType == static::REPEAT_WEEKLY_TEXT) {
			$days = [];
			foreach($event->getOnDays() as $day => $isEnabled) {
				if ($isEnabled) {
					$days[] = self::DAY_TEXT[$day];
				}
			}
			if (!empty($days)) {
				$result .= ';BYDAY=' . implode(',', $days);
			}
		}

		$this->writeln($out, $result);

		foreach ($event->getExcludedDates() as $exDt) {
			$this->writeln($out, 'EXDATE:' . $this->getDateText($exDt));
		}
	}

	protected function getDateText(\DateTime $dt) {
		if ($dt->getTimeZone()->getName() === 'UTC') {
			return $this->getDateUTCText($dt);
		}
		return $this->getDateWithTimeZoneText($dt);
	}

	protected function getDateWithTimeZoneText(\DateTime $dt) {
		return 'TZID=' .
			$dt->getTimeZone()->getName() .
			':' .
			$dt->format(static::DATE_FORMAT)
		;
	}

	protected function getDateUTCText(\DateTime $dt, bool $force = false) {
		if ($force && $dt->getTimeZone()->getName() != 'UTC') {
			$dt->setTimeZone(new \DateTimeZone('UTC'));
		}
		return $dt->format(static::DATE_FORMAT) . 'Z';
	}

	protected function generateUID() {
		return $this->outputTime->format(static::DATE_FORMAT) . 'Z-' . ($this->uidCounter++) . '@' . ($_SERVER['HTTP_HOST'] ?? 'localhost');
	}
}