<?php
// © 2017 Thomas Meester

namespace CalendarOutput;

interface OutputInterface {
	public function getHeaders():array; // should return an array of strings of headers that are required for the calendar to work, e.g. a content-type header
	public function printCalendar(\Calendar\Calendar $cal, $out = STDOUT):bool; //print calendar to stream $out, returns if successful
}