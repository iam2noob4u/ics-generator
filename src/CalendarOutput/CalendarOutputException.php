<?php
// © 2017 Thomas Meester

namespace CalendarOutput;

use \Calendar\Calendar;
use \Calendar\Event;

class CalendarOutputException extends \RuntimeException {}