<?php
// © 2017 Thomas Meester

namespace Calendar;

class Event {
	const REPEAT_NONE = 0;
	const REPEAT_YEARLY = 1;
	const REPEAT_MONTHLY = 2;
	const REPEAT_WEEKLY = 3;
	const REPEAT_DAILY = 4;
	const REPEAT_HOURLY = 5;
	const REPEAT_MINUTELY = 6;
	const REPEAT_SECONDLY = 7;

	const DAY_SUNDAY = 0;
	const DAY_MONDAY = 1;
	const DAY_TUESDAY = 2;
	const DAY_WEDNESDAY = 3;
	const DAY_THURSDAY = 4;
	const DAY_FRIDAY = 5;
	const DAY_SATURDAY = 6;

	protected $startDate;
	protected $endDate;
	protected $name;
	protected $uid = null;
	protected $location = null;
	protected $description = null;
	protected $repeatMode = self::REPEAT_NONE;
	protected $repeatInterval = 1; // once very <repeatInterval> day/week/month/etc
	protected $endRepeat = null; // datetime for "until", int for amount

	protected $onDays = [
		self::DAY_SUNDAY => false,
		self::DAY_MONDAY => false,
		self::DAY_TUESDAY => false,
		self::DAY_WEDNESDAY => false,
		self::DAY_THURSDAY => false,
		self::DAY_FRIDAY => false,
		self::DAY_SATURDAY => false,
	];

	protected $excludeDates = [];

	protected $isAdaptedVersionOf = null;
	protected $wouldNormallyHaveStartDate = null;

	public function __construct(\DateTime $start, \DateTime $end, string $name) {
		$this->startDate = $start;
		$this->endDate = $end;
		$this->name = $name;
	}

	public function createAlternateDuration(\DateTime $normalStartDate, \DateTime $alternateStartDate = null, \DateTime $alternateEndDate = null) {
		$clone = clone $this;

		if ($alternateStartDate !== null) {
			$clone->startDate = $alternateStartDate;
		}

		if ($alternateEndDate !== null) {
			$clone->endDate = $alternateEndDate;
		}

		$clone->repeatMode = static::REPEAT_NONE;
		$clone->endRepeat = null; // not really that important
		$clone->repeatInterval = 1; // not really that important
		$clone->excludeDates = [];

		$clone->isAdaptedVersionOf = $this;
		$clone->wouldNormallyHaveStartDate = $normalStartDate;

		return $clone;
	}

	public function getBasedOff() {
		return $this->isAdaptedVersionOf;
	}

	public function getNormalStartTime() {
		return $this->wouldNormallyHaveStartDate;
	}

	public function getStartDate():\DateTime {
		return $this->startDate;
	}

	public function setStartDate(\DateTime $startDate):self {
		$this->startDate = $startDate;

		return $this;
	}

	public function getEndDate():\DateTime {
		return $this->endDate;
	}

	public function setEndDate(\DateTime $endDate):self {
		$this->endDate = $endDate;

		return $this;
	}

	public function getName():string {
		return $this->name;
	}

	public function setName(string $name):self {
		$this->name = $name;

		return $this;
	}

	public function getUID() {
		return $this->uid;
	}

	public function setUID(string $uid):self {
		$this->uid = $uid;

		return $this;
	}

	public function getLocation():string {
		return $this->location;
	}

	public function setLocation(string $location):self {
		$this->location = $location;

		return $this;
	}

	public function getDescription() {
		return $this->description;
	}

	public function setDescription(string $description):self {
		$this->description = $description;

		return $this;
	}

	public function getRepeatMode():int {
		return $this->repeatMode;
	}

	public function setRepeatMode(int $repeatMode):self {
		$this->repeatMode = $repeatMode;

		return $this;
	}

	public function getRepeatInterval():int {
		return $this->repeatInterval;
	}

	public function setRepeatInterval(int $repeatInterval):self {
		$this->repeatInterval = $repeatInterval;

		return $this;
	}

	public function getEndRepeat() {
		return $this->endRepeat;
	}

	public function setEndRepeat($endRepeat):self {
		$this->endRepeat = $endRepeat;

		return $this;
	}

	public function excludeDate(\DateTime $dt):self {
		$this->excludeDates[] = $dt;

		return $this;
	}

	public function unexcludeDate(\DateTime $dt, bool $strict = true):self {
		$idx = array_search($dt, $this->excludedDates, $strict);
		if ($idx !== false) {
			array_splice($this->excludedDates, $idx, 1);
		}

		return $this;
	}

	public function isDateExcluded(\DateTime $dt, bool $strict = true):bool {
		return in_array($dt, $this->excludedDates, $strict);
	}

	public function getExcludedDates() {
		return $this->excludeDates;
	}

	public function setOnDay(int $day, bool $status):self {
		if (isset($this->onDays[$day])) {
			$this->onDays[$day] = $status;
		}

		return $this;
	}

	public function isOnDay(int $day):bool {
		return $this->onDays[$day] ?? false;
	}

	public function getOnDay(int $day):bool {
		return $this->isOnDay($day);
	}

	public function getOnDays():array {
		return $this->onDays;
	}
}