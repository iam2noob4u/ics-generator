<?php
// © 2017 Thomas Meester

namespace Calendar;

class Calendar {
	protected $name = '';
	protected $defaultTimeZone;
	protected $description = '';
	protected $events = [];

	public function __construct(\DateTimeZone $defaultTimeZone = null) {
		$this->defaultTimeZone = $defaultTimeZone ?? new \DateTimeZone('UTC');
	}

	public function getName():string {
		return $this->name;
	}

	public function setName(string $name):self {
		$this->name = $name;

		return $this;
	}

	public function getDefaultTimeZone():\DateTimeZone {
		return $this->defaultTimeZone;
	}

	public function setDefaultTimeZone(\DateTimeZone $defaultTimeZone):self {
		$this->defaultTimeZone = $defaultTimeZone;

		return $this;
	}

	public function getDescription():string {
		return $this->description;
	}

	public function setDescription(string $description):self {
		$this->description = $description;

		return $this;
	}

	public function addEvent(Event $event):self {
		if (!$this->hasEvent($event)) {
			$this->events[] = $event;
		}

		return $this;
	}

	public function hasEvent(Event $event):bool {
		return in_array($event, $this->events, true);
	}

	public function removeEvent(Event $event):self {
		$idx = array_search($event, $this->events, true);
		if ($idx !== false) {
			array_splice($this->events, $idx, 1);
		}

		return $this;
	}

	public function clearEvents():self {
		$this->events = [];

		return $this;
	}

	public function getEvents():array {
		return $this->events;
	}

	public function getEventCount():int {
		return count($this->events);
	}

	public function getEvent(int $idx):Event {
		return $this->events[$idx];
	}
}