<?php

// require __DIR__ . '/vendor/autoload.php'; // or something like that

// this will work in this repository as-is (not downloaded through composer)
require __DIR__ . '/../src/Calendar/Calendar.php';
require __DIR__ . '/../src/Calendar/Event.php';
require __DIR__ . '/../src/CalendarOutput/CalendarOutputException.php';
require __DIR__ . '/../src/CalendarOutput/OutputInterface.php';
require __DIR__ . '/../src/CalendarOutput/ICSOutput.php';

$cal = new \Calendar\Calendar(new \DateTimeZone('Europe/Amsterdam'));
$event = new \Calendar\Event(new \DateTime('2017-01-01 09:00:00'), new \DateTime('2017-01-01 12:00:00'), "wekelijks oneindig");
$event->setRepeatMode(\Calendar\Event::REPEAT_WEEKLY);
$event->excludeDate(new \DateTime('2017-01-29 09:00:00'));
$cal->addEvent($event);
$adaption = $event->createAlternateDuration(new \DateTime('2017-01-15 09:00:00'), new \DateTime('2017-01-15 08:00:00'), new \DateTime('2017-01-15 11:00:00'));
$cal->addEvent($adaption);
$event = new \Calendar\Event(new \DateTime('2017-01-05 09:00:00'), new \DateTime('2017-01-05 12:00:00'), "eenmalig op een andere dag");
$cal->addEvent($event);
$event = new \Calendar\Event(new \DateTime('2017-02-01 09:00:00'), new \DateTime('2017-02-01 10:00:00'), "elke maandag en woensdag om 9:00 voor de hele maand");
$event->setRepeatMode(\Calendar\Event::REPEAT_WEEKLY);
$event->setEndRepeat(new \DateTime('2017-02-28 23:59:59'));
$event->setOnDay(\Calendar\Event::DAY_MONDAY, true)->setOnDay(\Calendar\Event::DAY_WEDNESDAY, true);
$cal->addEvent($event);
$output = new \CalendarOutput\ICSOutput();

$f = fopen('testoutput.ics', 'w');
$output->printCalendar($cal, $f);
fflush($f);
fclose($f);
var_dump(file_get_contents('testoutput.ics'));